import $ from 'jquery'
import 'bootstrap/dist/css/bootstrap.min.css'
import '../styles/index.scss'

/**
 * Global Constants
 */
const userUrl = 'https://jsonplaceholder.typicode.com/users/';
const postsUrl = 'https://jsonplaceholder.typicode.com/posts/';
/**
 * Variables
 */

/**
 * Wait for the DOM to be ready
 * jQuery needs to cache to DOM :)
 */
class Post {
    constructor(post) {
        this.img = post.img;
        this.id = post.id;
        this.userId = post.userId;
        this.title = post.title;
        this.body = post.body;
    }
}

class User {
    constructor(user) {
        this.id = user.id;
        this.name = user.name;
        this.username = user.username;
        this.email = user.email;
        this.address = user.address;
    }
}


const postArray = [];
const userArray = [];

$(document).ready(function () {
    
    //Get all users and add to array userArray
    const loadUsers = $.ajax({
        url: userUrl,
        method: 'GET',
        success: function(data) {
            $(data).each((index ,user) => {
                const newUser = new User(user);
                userArray.push(newUser);
            });
        },
    });

    //Get all posts, add posts to postArray, and add onClicks
    const loadPosts = $.ajax({
        url: postsUrl,
        method: 'GET',
        success: function(data) {
            $(data).each((index ,post) => {
                //Creates a separate Post object, and appends it to postArray
                let defaultImg = 'https://studiok40.com/wp-content/uploads/2018/01/open-graph-protocol-logo-article-image.gif';
                post.img = defaultImg;                
                const elPost = createCard(post.img, post.title, post.body, post.id);
                $(elPost).appendTo('.post-elem');  

                
                const idOfPost = '.post-'+ post.id;
                let blogsUser = "";
                userArray.forEach((user) => {
                    if (user.id === post.userId) {
                        blogsUser = user;
                    };
                });

                //Makes a click event for the post that makes it show only that post
                $(idOfPost).click(() => {
                    showOnlyOnePost(post, blogsUser);
                });
            });
        },
    });

    
});

/**
 * Build a Card template with a picture.
 * @param {string|null} photo
 * @param {string} title
 * @param {string} body
 * @returns String
 */
function createCard(url, title, body, id) {
    return `
        <div class="col-4 post-${id}">
            <div class="card h-100" style="18rem;">
                ${url
                    ? `
                        <div class="card-img">
                            <img class="card-img-top" src="${url}"/>
                        </div>
                      ` : ''}
                <div class="card-body">
                    <h5>${title}</h5>
                </div>
            </div>
        </div>
    `;
}

function showOnlyOnePost(post, user) {
    $('.post-elem').hide();
    const detailedPost = detailedCard(post, user);
    $(detailedPost).appendTo('.detailed-post');

}

//Makes the html for a blog post with details
function detailedCard(post, user) {
    return `
        <div class="col-4" id="${post.id}">
        <div class="card h-100" style="18rem;">
            ${post.img
                ? `
                    <div class="card-img">
                        <img class="card-img-top" src="${post.img}"/>
                    </div>
                ` : ''}
            <div class="card-body">
                <h5>${post.title}</h5>
                ${post.body.split('\n').map(para => `<p>${para}</p>`).join('\n')}
                <div class="card-subtitle mb-2 text-muted">${user.username}</div>
            </div>

        </div>
        </div>
    `;
}